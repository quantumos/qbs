#include <stdio.h>
#include <errno.h>
#include <unistd.h>

#include <xf86drm.h>
#include <xf86drmMode.h>

#include "include/drmutil.h"
#include "include/render.h"

int main(int argc, char **argv) {
    struct qbs_drm_dev *dev_list = NULL;
    
    int ret, fd;
    const char *card;
    struct qbs_drm_dev *iter;
    struct qbs_drm_buf *buf;

    /* check which DRM device to open */
    if (argc > 1)
        card = argv[1];
    else
        card = "/dev/dri/card0";

    fprintf(stderr, "using card '%s'\n", card);

    /* open the DRM device */
    ret = qbs_drm_open(&fd, card);
    if (ret) {
        goto out_return;
    }

    /* prepare all connectors and CRTCs */
    ret = qbs_drm_prepare(fd, &dev_list);
    if (ret)
        goto out_close;

    /* perform actual modesetting on each found connector+CRTC */
    for (iter = dev_list;iter;iter = iter->next) {
        iter->saved_crtc = drmModeGetCrtc(fd, iter->crtc);
        buf = &iter->bufs[iter->front_buf];
        ret = drmModeSetCrtc(fd, iter->crtc, buf->fb, 0, 0,
                &iter->conn, 1, &iter->mode);
        if (ret)
            fprintf(stderr, "cannot set CRTC for connector %u (%d): %m\n",
                    iter->conn, errno);
    }

    /* draw some colors for 5 seconds */
    qbs_rd_render(fd, &dev_list);

    /* cleanup everything */
    qbs_drm_cleanup(fd, &dev_list);

    ret = 0;

out_close:
    close(fd);
out_return:
    if (ret) {
        errno = -ret;
        fprintf(stderr, "modeset failed with error %d: %m\n", errno);
    } else {
        fprintf(stderr, "exiting\n");
    }
    return ret;
}
