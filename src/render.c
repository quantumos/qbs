#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#include "include/drmutil.h"
#include "include/artist.h"
#include "include/render.h"

uint8_t next_color(bool *up, uint8_t cur, unsigned int mod)
{
    uint8_t next;

    next = cur + (*up ? 1 : -1) * (rand() % mod);
    if ((*up && next < cur) || (!*up && next > cur)) {
        *up = !*up;
        next = cur;
    }

    return next;
}

void qbs_rd_render(int fd, struct qbs_drm_dev **dev_list)
{
    int ret;
    fd_set fds;
    time_t start, cur;
    struct timeval v;
    drmEventContext ev;
    struct qbs_drm_dev *iter;

    /* init variables */
    srand(time(&start));
    FD_ZERO(&fds);
    memset(&v, 0, sizeof(v));
    memset(&ev, 0, sizeof(ev));
    /* Set this to only the latest version you support. Version 2
     * introduced the page_flip_handler, so we use that. */
    ev.version = 2;
    ev.page_flip_handler = qbs_drm_page_flip_event;

    /* redraw all outputs */
    for (iter = *dev_list;iter;iter = iter->next) {
        qbs_art_draw_clear(&iter->bufs[0], iter->art_state.bg);
        qbs_art_draw_clear(&iter->bufs[1], iter->art_state.bg);
        qbs_rd_render_dev(fd, iter);
    }

    int wait = 10;
    /* wait 5s for VBLANK or input events */
    while (time(&cur) < start + wait) {
        FD_SET(0, &fds);
        FD_SET(fd, &fds);
        v.tv_sec = start + wait - cur;

        ret = select(fd + 1, &fds, NULL, NULL, &v);
        if (ret < 0) {
            fprintf(stderr, "select() failed with %d: %m\n", errno);
            break;
        } else if (FD_ISSET(0, &fds)) {
            fprintf(stderr, "exit due to user-input\n");
            break;
        } else if (FD_ISSET(fd, &fds)) {
            drmHandleEvent(fd, &ev);
        }
    }
}

void qbs_rd_render_dev(int fd, struct qbs_drm_dev *dev)
{
    struct timespec start, stop;
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start);

    struct qbs_drm_buf *buf;
    int ret;

    buf = &dev->bufs[dev->front_buf ^ 1];
    unsigned int w = buf->width, h = buf->height;

    qbs_art_draw_prog(buf, w/2, 2*h/3, w/4, 20,
            2, dev->art_state.bg, dev->art_state.fg, 
            100, dev->art_state.prog - 100, dev->art_state.fade);
    dev->art_state.prog = ((dev->art_state.prog + 5) % (w/4 + 100));
    if (dev->art_state.fade < 1) dev->art_state.fade += 0.02;

    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &stop);

    double result = stop.tv_nsec - start.tv_nsec;//(stop.tv_sec - start.tv_sec) * 1e9 + (stop.tv_nsec - start.tv_nsec) / 1e3;
    /*printf("%f\n", result);*/

    ret = drmModePageFlip(fd, dev->crtc, buf->fb,
            DRM_MODE_PAGE_FLIP_EVENT, dev);

    if (ret)
        fprintf(stderr, "cannot flip CRTC for connector %u (%d): %m\n",
                dev->conn, errno);
    else {
        dev->front_buf ^= 1;
        dev->pflip_pending = true;
    }
}
