// artist - draws shapes, widgets, etc. onto the buffer

#ifndef _QBS_ARTIST_H
#define _QBS_ARTIST_H

#include <stdint.h>

#include "include/structs.h"
#include "include/drmutil.h"

void qbs_art_draw_clear(struct qbs_drm_buf *buf, struct qbs_art_color color);
void qbs_art_draw_rect(struct qbs_drm_buf *buf, int x, int y, int w, int h,
        struct qbs_art_color color);
void qbs_art_blend_rect(struct qbs_drm_buf *buf, int x, int y, int w, int h,
        struct qbs_art_color bg, int percent);
void qbs_art_draw_prog(struct qbs_drm_buf *buf, int cx, int cy, int w, int h,
        int bd, struct qbs_art_color bg, struct qbs_art_color fg,
        int width, int prog, double fade);

#endif
