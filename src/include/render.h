#ifndef _QBS_RENDER_H
#define _QBS_RENDER_H

#include <stdbool.h>

#include "include/artist.h"
#include "include/drmutil.h"

struct qbs_rd_data {
    struct qbs_art_state *state;
    struct qbs_drm_dev *dev;
};

uint8_t next_color(bool *up, uint8_t cur, unsigned int mod);
void qbs_rd_render(int fd, struct qbs_drm_dev **dev_list);
void qbs_rd_render_dev(int fd, struct qbs_drm_dev *dev);

#endif
