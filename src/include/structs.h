#ifndef _QBS_STRUCTS_H
#define _QBS_STRUCTS_H

#include <xf86drm.h>
#include <xf86drmMode.h>

#include <stdint.h>
#include <stdbool.h>

enum qbs_art_stage {
    QBS_ART_STAGE_BGIN,
    QBS_ART_STAGE_FGIN,
    QBS_ART_STAGE_PROG,
    QBS_ART_STAGE_FGOUT,
    QBS_ART_STAGE_BGOUT,
};

struct qbs_art_color {
    uint8_t r;
    uint8_t g;
    uint8_t b;
};

struct qbs_art_state {
    enum qbs_art_stage stage;
    struct qbs_art_color bg, fg;
    int prog;
    double fade;
};

struct qbs_drm_buf {
    uint32_t width, height, stride, size, handle;
    uint8_t *map;
    uint32_t fb;
};

struct qbs_drm_dev {
    struct qbs_drm_dev *next;

    unsigned int front_buf;
    struct qbs_drm_buf bufs[2];

    drmModeModeInfo mode;
    uint32_t conn, crtc;
    drmModeCrtc *saved_crtc;

    bool pflip_pending;
    bool cleanup;

    uint8_t r, g, b;
    bool r_up, g_up, b_up;

    struct qbs_art_state art_state;
};

struct qbs_art_rect {
    unsigned int x, y, width, height;
    unsigned int bd_thick, bd_rad;
    struct qbs_art_color bg, fg;
};

#endif
