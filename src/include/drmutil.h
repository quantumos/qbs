#ifndef _QBS_DRMUTIL_H
#define _QBS_DRMUTIL_H

#include <stdbool.h>

#include <xf86drm.h>
#include <xf86drmMode.h>

#include "include/structs.h"
#include "include/artist.h"

int qbs_drm_open(int *out, const char *node);
int qbs_drm_prepare(int fd, struct qbs_drm_dev **dev_list);
int qbs_drm_setup_dev(int fd, drmModeRes *res, drmModeConnector *conn,
        struct qbs_drm_dev *dev, struct qbs_drm_dev *dev_list);
int qbs_drm_find_crtc(int fd, drmModeRes *res, drmModeConnector *conn,
            struct qbs_drm_dev *dev, struct qbs_drm_dev *dev_list);
int qbs_drm_create_fb(int fd, struct qbs_drm_buf *buf);
void qbs_drm_destroy_fb(int fd, struct qbs_drm_buf *buf);
void qbs_drm_page_flip_event(int fd, unsigned int frame,
        unsigned int sec, unsigned int usec,
        void *data);
void qbs_drm_cleanup(int fd, struct qbs_drm_dev **dev_list);

#endif
