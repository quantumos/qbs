#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <sys/mman.h>
#include <time.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>

#include <xf86drm.h>
#include <xf86drmMode.h>

#include "include/render.h"
#include "include/artist.h"
#include "include/drmutil.h"

int qbs_drm_open(int *out, const char *node)
{
    int fd, ret;
    uint64_t has_dumb;

    fd = open(node, O_RDWR | O_CLOEXEC);
    if (fd < 0) {
        ret = -errno;
        fprintf(stderr, "cannot open '%s': %m\n", node);
        return ret;
    }

    if (drmGetCap(fd, DRM_CAP_DUMB_BUFFER, &has_dumb) < 0 ||
            !has_dumb) {
        fprintf(stderr, "drm device '%s' does not support dumb buffers\n",
                node);
        close(fd);
        return -EOPNOTSUPP;
    }

    *out = fd;
    return 0;
}

int qbs_drm_prepare(int fd, struct qbs_drm_dev **dev_list)
{
    drmModeRes *res;
    drmModeConnector *conn;
    unsigned int i;
    struct qbs_drm_dev *dev;
    int ret;

     /* retrieve resources */
    res = drmModeGetResources(fd);
    if (!res) {
        fprintf(stderr, "cannot retrieve DRM resources (%d): %m\n",
                errno);
        return -errno;
    }

    /* iterate all connectors */
    for (i = 0;i < res->count_connectors;i++) {
        /* get information for each connector */
        conn = drmModeGetConnector(fd, res->connectors[i]);
        if (!conn) {
            fprintf(stderr, "cannot retrieve DRM connector %u:%u (%d)",
                    i, res->connectors[i], errno);
            continue;
        }
        /* create a device structure */
        dev = malloc(sizeof(*dev));
        memset(dev, 0, sizeof(*dev));
        dev->conn = conn->connector_id;

        /* call helper function to prepare this connector */
        ret = qbs_drm_setup_dev(fd, res, conn, dev, *dev_list);
        if (ret) {
            if (ret != -ENOENT) {
                errno = -ret;
                fprintf(stderr, "cannot setup device for connector %u:%u (%d): %m\n",
                        i, res->connectors[i], errno);
            }
            free(dev);
            drmModeFreeConnector(conn);
            continue;
        }

        /* free connector data and link device into list */
        drmModeFreeConnector(conn);
        dev->next = *dev_list;
        *dev_list = dev;
    }

    /* free resources again */
    drmModeFreeResources(res);

    return 0;
}

int qbs_drm_setup_dev(int fd, drmModeRes *res, drmModeConnector *conn,
        struct qbs_drm_dev *dev, struct qbs_drm_dev *dev_list)
{
    int ret;

    /* check if a monitor is connected */
    if (conn->connection != DRM_MODE_CONNECTED) {
        fprintf(stderr, "ignoring unused connector %u\n",
                conn->connector_id);
        return -ENOENT;
    }

    /* check if there is at least one valid mode */
    if (conn->count_modes == 0) {
        fprintf(stderr, "no valid mode for connector %u\n",
                conn->connector_id);
        return -EFAULT;
    }

    /* copy the mode information into our device structure */
    memcpy(&dev->mode, &conn->modes[0], sizeof(dev->mode));
    dev->bufs[0].width = conn->modes[0].hdisplay;
    dev->bufs[0].height = conn->modes[0].vdisplay;
    dev->bufs[1].width = conn->modes[0].hdisplay;
    dev->bufs[1].height = conn->modes[0].vdisplay;
    fprintf(stdout, "mode for connector %u is %ux%u\n",
            conn->connector_id, dev->bufs[0].width, dev->bufs[0].height);

    /* find a crtc for this connector */
    ret = qbs_drm_find_crtc(fd, res, conn, dev, dev_list);
    if (ret) {
        fprintf(stderr, "no valid crtc for connector %u\n",
                conn->connector_id);
        return ret;
    }

    /* create framebuffer #1 for this CRTC */
    ret = qbs_drm_create_fb(fd, &dev->bufs[0]);
    if (ret) {
        fprintf(stderr, "cannot create framebuffer for connector %u\n",
                conn->connector_id);
        return ret;
    }

    /* create framebuffer #2 for this CRTC */
    ret = qbs_drm_create_fb(fd, &dev->bufs[1]);
    if (ret) {
        fprintf(stderr, "cannot create framebuffer for connector %u\n",
                conn->connector_id);
        qbs_drm_destroy_fb(fd, &dev->bufs[0]);
        return ret;
    }

    struct qbs_art_state state = {
        .bg = {.r = 0x11, .g = 0x23, .b=0x32},
        .fg = {.r = 0x47, .g = 0xAE, .b = 0x84},
        .stage = QBS_ART_STAGE_BGIN,
        .prog = 0,
        .fade = 0.0,
    };
    dev->art_state = state;

    return 0;
}

int qbs_drm_create_fb(int fd, struct qbs_drm_buf *buf)
{
    struct drm_mode_create_dumb creq;
    struct drm_mode_destroy_dumb dreq;
    struct drm_mode_map_dumb mreq;
    int ret;

    /* create dumb buffer */
    memset(&creq, 0, sizeof(creq));
    creq.width = buf->width;
    creq.height = buf->height;
    creq.bpp = 32;
    ret = drmIoctl(fd, DRM_IOCTL_MODE_CREATE_DUMB, &creq);
    if (ret < 0) {
        fprintf(stderr, "cannot create dumb buffer (%d): %m\n",
                errno);
        return -errno;
    }
    buf->stride = creq.pitch;
    buf->size = creq.size;
    buf->handle = creq.handle;

    /* create framebuffer object for the dumb-buffer */
    ret = drmModeAddFB(fd, buf->width, buf->height, 24, 32, buf->stride,
            buf->handle, &buf->fb);
    if (ret) {
        fprintf(stderr, "cannot create framebuffer (%d): %m\n",
                errno);
        ret = -errno;
        goto err_destroy;
    }

    /* prepare buffer for memory mapping */
    memset(&mreq, 0, sizeof(mreq));
    mreq.handle = buf->handle;
    ret = drmIoctl(fd, DRM_IOCTL_MODE_MAP_DUMB, &mreq);
    if (ret) {
        fprintf(stderr, "cannot map dumb buffer (%d): %m\n",
                errno);
        ret = -errno;
        goto err_fb;
    }

    /* perform actual memory mapping */
    buf->map = mmap(0, buf->size, PROT_READ | PROT_WRITE, MAP_SHARED,
            fd, mreq.offset);
    if (buf->map == MAP_FAILED) {
        fprintf(stderr, "cannot mmap dumb buffer (%d): %m\n",
                errno);
        ret = -errno;
        goto err_fb;
    }

    /* clear the framebuffer to 0 */
    memset(buf->map, 0, buf->size);

    return 0;

err_fb:
    drmModeRmFB(fd, buf->fb);
err_destroy:
    memset(&dreq, 0, sizeof(dreq));
    dreq.handle = buf->handle;
    drmIoctl(fd, DRM_IOCTL_MODE_DESTROY_DUMB, &dreq);
    return ret;
}

void qbs_drm_destroy_fb(int fd, struct qbs_drm_buf *buf)
{
    struct drm_mode_destroy_dumb dreq;

    /* unmap buffer */
    munmap(buf->map, buf->size);

    /* delete framebuffer */
    drmModeRmFB(fd, buf->fb);

    /* delete dumb buffer */
    memset(&dreq, 0, sizeof(dreq));
    dreq.handle = buf->handle;
    drmIoctl(fd, DRM_IOCTL_MODE_DESTROY_DUMB, &dreq);
}

int qbs_drm_find_crtc(int fd, drmModeRes *res, drmModeConnector *conn,
        struct qbs_drm_dev *dev, struct qbs_drm_dev *dev_list)
{
    drmModeEncoder *enc;
    unsigned int i, j;
    int32_t crtc;
    struct qbs_drm_dev *iter;

    /* first try the currently connected encoder+crtc */
    if (conn->encoder_id)
        enc = drmModeGetEncoder(fd, conn->encoder_id);
    else
        enc = NULL;

    if (enc) {
        if (enc->crtc_id) {
            crtc = enc->crtc_id;
            for (iter = dev_list;iter;iter = iter->next) {
                if (iter->crtc == crtc) {
                    crtc = -1;
                    break;
                }
            }

            if (crtc >= 0) {
                drmModeFreeEncoder(enc);
                dev->crtc = crtc;
                return 0;
            }
        }

        drmModeFreeEncoder(enc);
    }

    /* If the connector is not currently bound to an encoder or if the
     * encoder+crtc is already used by another connector (actually unlikely
     * but lets be safe), iterate all other available encoders to find a
     * matching CRTC. */
    for (i = 0;i < conn->count_encoders;i++) {
        enc = drmModeGetEncoder(fd, conn->encoders[i]);
        if (!enc) {
            fprintf(stderr, "cannot retrieve encoder %u:%u (%d): %m\n",
                    i, conn->encoders[i], errno);
            continue;
        }

        /* iterate all global CRTCs */
        for (j = 0;j < res->count_crtcs;j++) {
            /* check whether this CRTC works with the encoder */
            if (!(enc->possible_crtcs & (1 << j)))
                continue;

            /* check that no other device already uses this CRTC */
            crtc = res->crtcs[j];
            for (iter = dev_list;iter;iter = iter->next) {
                if (iter->crtc == crtc) {
                    crtc = -1;
                    break;
                }
            }

            /* we have found a CRTC, so save it and return */
            if (crtc >= 0) {
                drmModeFreeEncoder(enc);
                dev->crtc = crtc;
                return 0;
            }
        }

        drmModeFreeEncoder(enc);
    }

    fprintf(stderr, "cannot find suitable CRTC for connector %u\n",
            conn->connector_id);
    return -ENOENT;
}

void qbs_drm_cleanup(int fd, struct qbs_drm_dev **dev_list)
{
    struct qbs_drm_dev *iter;
    drmEventContext ev;
    int ret;
    /* init variables */
    memset(&ev, 0, sizeof(ev));
    ev.version = DRM_EVENT_CONTEXT_VERSION;
    ev.page_flip_handler = qbs_drm_page_flip_event;

    while (*dev_list) {
        /* remove from global list */
        iter = *dev_list;
        *dev_list = iter->next;

        /* if a pageflip is pending, wait for it to complete */
        iter->cleanup = true;
        fprintf(stderr, "wait for pending page-flip to complete...\n");
        while (iter->pflip_pending) {
            ret = drmHandleEvent(fd, &ev);
            if (ret)
                break;
        }
        
        /* restore saved CRTC configuration */
        if (!iter->pflip_pending)
            drmModeSetCrtc(fd,
                    iter->saved_crtc->crtc_id,
                    iter->saved_crtc->buffer_id,
                    iter->saved_crtc->x,
                    iter->saved_crtc->y,
                    &iter->conn,
                    1,
                    &iter->saved_crtc->mode);
        drmModeFreeCrtc(iter->saved_crtc);
        
        /* destroy framebuffers */
        qbs_drm_destroy_fb(fd, &iter->bufs[1]);
        qbs_drm_destroy_fb(fd, &iter->bufs[0]);

        /* free allocated memory */
        free(iter);
    }
}

void qbs_drm_page_flip_event(int fd, unsigned int frame,
        unsigned int sec, unsigned int usec,
        void *data)
{
    struct qbs_drm_dev *dev = data;

    dev->pflip_pending = false;
    if (!dev->cleanup)
        qbs_rd_render_dev(fd, dev);
}
