#include <stdio.h>
#include <math.h>

#include "include/drmutil.h"
#include "include/artist.h"

#define OFFSET(s,i,j) s * (i) + (j) * 4
#define MIN(a,b) (a) < (b) ? (a) : (b)
#define MAX(a,b) (a) > (b) ? (a) : (b)

void qbs_art_draw_clear(struct qbs_drm_buf *buf, struct qbs_art_color color)
{
    unsigned int i, j, off;
    for (i = 0;i < buf->height;i++) {
        for (j = 0;j < buf->width;j++) {
            off = buf->stride * i + j * 4;
            *(uint32_t*)&buf->map[off] = 
                (color.r << 16) | (color.g << 8) | color.b;
        }
    }
}

void qbs_art_draw_rect(struct qbs_drm_buf *buf, int x, int y, int w, int h,
        struct qbs_art_color color)
{
    unsigned int i, j, off;

    for (j = 0;j < h;j++) {
        for (i = 0;i < w;i++) {
            off = OFFSET(buf->stride, j + y, i + x);
            *(uint32_t*)&buf->map[off] = 
                (color.r << 16) | (color.g << 8) | color.b;
        }
    }
}

void qbs_art_blend_rect(struct qbs_drm_buf *buf, int x, int y, int w, int h,
        struct qbs_art_color bg, int percent)
{
    unsigned int i, j, off;
    for (j = 0;j < h;j++) {
        for (i = 0;i < w;i++) {
            off = OFFSET(buf->stride, j + y, i + x);
            *(uint32_t*)&buf->map[off] *=
                (percent / 100);
        }
    }
}

void qbs_art_draw_prog(struct qbs_drm_buf *buf, int cx, int cy, int w, int h,
        int bd, struct qbs_art_color bg, struct qbs_art_color fg, 
        int width, int prog, double fade)
{
    int x = cx - w/2;
    int y = cy - h/2;

    fg.r = (fg.r * fade) + (bg.r * (1 - fade));
    fg.g = (fg.g * fade) + (bg.g * (1 - fade));
    fg.b = (fg.b * fade) + (bg.b * (1 - fade));
    qbs_art_draw_rect(buf, x, y, w, h, fg);
    qbs_art_draw_rect(buf, x + bd, y + bd, w - bd - bd, h - bd - bd, bg);
    int px = MAX(prog, 0);
    int px2 = MIN(MAX(prog + width, 0), w);
    int pwidth = px2 - px;
    qbs_art_draw_rect(buf, x + px, y + bd, pwidth, h - bd - bd, fg);
    /*qbs_art_blend_rect(buf, x, y, w, h, bg, 10);*/
}
